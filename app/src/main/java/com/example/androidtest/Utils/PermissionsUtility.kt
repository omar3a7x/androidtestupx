package com.example.androidtest.Utils

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

object PermissionsUtility {

    enum class Codes(val code: Int) {
        FOR_READ(100),
        FOR_WRITE(101),
    }

    fun askPermissionIfNotGranted(activity: AppCompatActivity, permission: String, permissionCode: Codes) {
        val permissionCodeValue = permissionCode.code

        if(!hasPermission(activity, permission)) {
            ActivityCompat.requestPermissions(activity, arrayOf(permission), permissionCodeValue)
        }
    }

    fun hasPermission(activity: AppCompatActivity, permission: String): Boolean {
        return (ContextCompat.checkSelfPermission(activity, permission)
                == PackageManager.PERMISSION_GRANTED)
    }

    fun isResultGranted(grantResults: IntArray, permission:String, activity: AppCompatActivity): Boolean {
        return (grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                hasPermission(activity, permission))
    }
}