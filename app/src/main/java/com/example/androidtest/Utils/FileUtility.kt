package com.example.androidtest.Utils

import android.content.Context
import android.net.Uri
import android.os.Environment
import android.util.Log
import java.io.*
import java.util.zip.ZipEntry
import java.util.zip.ZipFile
import java.util.zip.ZipInputStream

class FileUtility {

    fun unzipFile(filePath: String) {
        val `is`: InputStream
        val zis: ZipInputStream
        try {

            val zipfile = File(filePath)
            val parentFolder = zipfile.getParentFile().getPath()
            var filename: String

            `is` = FileInputStream(filePath)
            zis = ZipInputStream(BufferedInputStream(`is`))
            var ze: ZipEntry?= null
            val buffer = ByteArray(1024)
            var count: Int

            ze = zis.nextEntry
            while (ze != null) {
                filename = ze.name

                if (ze.isDirectory) {
                    val fmd = File(parentFolder + "/" + filename)
                    fmd.mkdirs()
                    continue
                }

                val fout = FileOutputStream(parentFolder + "/" + filename)

                count = zis.read(buffer)
                while (count != -1) {
                    fout.write(buffer, 0, count)
                    count = zis.read(buffer)
                }

                fout.close()
                zis.closeEntry()
                ze = zis.nextEntry
            }

            zis.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    public fun readTextFile(uri: Uri, context: Context): String{
        var reader: BufferedReader? = null;
        var builder: StringBuilder? = StringBuilder();
        try {
            reader = BufferedReader(InputStreamReader(context.getContentResolver().openInputStream(uri)));
            var line: String? = "";

            line = reader.readLine()
            while (line != null) {
                builder!!.append(line)
                line = reader.readLine()
            }

        } catch (e: IOException) {
            e.printStackTrace();
        } finally {
            if (reader != null){
                try {
                    reader.close();
                } catch (e: IOException) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }
}