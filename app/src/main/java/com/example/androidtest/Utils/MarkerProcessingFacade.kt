package com.example.androidtest.Utils

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.webkit.MimeTypeMap
import android.webkit.URLUtil
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.androidtest.Data.DataRepository
import com.example.androidtest.Data.FetchStrategies.DownloadManagerStrategy
import com.example.androidtest.Data.FetchStrategies.RemoteFetchStrategy
import com.example.androidtest.Data.Models.MapMarkerModel
import com.example.androidtest.Data.Models.SingleData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.io.File
import kotlin.coroutines.CoroutineContext

/**
 *  Facade for map markers processing
 *  Exposed through ProcessHandler interface when processing is finished
 *  Hides the logic of
 *      1. fetch zip file link via Retrofit fetch strategy
 *      2. fetch zip file via Download manager fetch strategy
 *      3. register for download of zip file broadcast
 *      4. unzip file
 *      5. create markers list from file content
 */

class MarkerProcessingFacade(val context:AppCompatActivity,
                             val handler: ProcessHandler) {
    private val TAG = "MarkerProcessingFacade"

    //  Coroutine init
    private val coroutineContext: CoroutineContext get() = Job() + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    //  Mutable data fetched from respositories
    private var singleData = MutableLiveData<SingleData>()
    private var downloadID = MutableLiveData<Long>()

    private var fileName = ""
    private var markers = ArrayList<MapMarkerModel>()

    //  When a download manager broadcast the download complete event
    private val onDownloadComplete = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            //  Id should be the zip file download signal
            val id = intent!!.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            if (downloadID.value == id) {
                //  4. unzip file
                unzipFile()
            }
        }

    }

    fun start() {
        //  1. fetch zip file link via Retrofit fetch strategy
        fetchZipFileLink()
    }

    private fun fetchZipFileLink() {
        //  Coroutine context for observing mutable data change
        scope.launch {
            val responseData = DataRepository(RemoteFetchStrategy()).getData()
            singleData.postValue(responseData)
            singleData.observe(context as LifecycleOwner, Observer {
                try {
                    val zipFileUrl = singleData.value!!.valueResponse
                    Log.e(TAG, zipFileUrl)

                    fileName = URLUtil.guessFileName(zipFileUrl, null, MimeTypeMap.getFileExtensionFromUrl(zipFileUrl))

                    //  2. fetch zip file via Download manager fetch strategy
                    fetchZipFile(zipFileUrl)
                } catch (e: Exception) {
                    //Log.e("MapRequestActivity", e.message)
                }
            })
        }
    }

    private fun fetchZipFile(zipFileUrl: String) {
        scope.launch {
            val fileRes = DataRepository(DownloadManagerStrategy(context)).getZipFile(zipFileUrl)
            downloadID.postValue(fileRes)
            downloadID.observe(context, Observer { id ->
                //  3. register download of zip file completed broadcast
                context.registerReceiver(
                    onDownloadComplete,
                    IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
                )
            })
        }
    }

    private fun unzipFile() {
        val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName)
        if(file.exists()){
            FileUtility().unzipFile(file.absolutePath)
            val unpackedFileName = file.absolutePath.replace("zip", "txt")
            val txtDoc = File(unpackedFileName)
            val content = FileUtility().readTextFile(Uri.fromFile(txtDoc), context)

            //  5. create markers list from file content
            val zipJson = JSONObject(content)
            try {
                val jaData = zipJson.getJSONArray("data")
                val joLocations = jaData.get(0) as JSONObject
                val jaLocations = joLocations.getJSONArray("UBICACIONES")
                for (i in 0 until jaLocations.length()) {
                    val currentLocation = jaLocations.get(i) as JSONObject
                    markers.add(
                        MapMarkerModel(
                            currentLocation.getDouble("FNLATITUD"),
                            currentLocation.getDouble("FNLONGITUD"),
                            currentLocation.getString("FCNOMBRE")
                        )
                    )

                }

                //  Finish
                handler.processFinish(markers, "")

            }catch(e: Exception) {
                Log.e("MapRequestActivity", "JSON " + e.message)
            }
        }
    }
}