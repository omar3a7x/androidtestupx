package com.example.androidtest.Utils

interface ProcessHandler {
    fun processFinish(result: Any, error: String)
}