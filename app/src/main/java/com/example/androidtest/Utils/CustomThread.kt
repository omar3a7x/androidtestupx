package com.example.androidtest.Utils

import java.lang.Thread.sleep

class CustomThread(val handler: ProcessHandler): Runnable {
    override fun run() {
        sleep(10000)
        handler.processFinish("", "")
    }
}