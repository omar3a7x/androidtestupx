package com.example.androidtest.Data.Models

data class MapMarkerModel(
    val latitude: Double,
    val longitude: Double,
    val title: String
)