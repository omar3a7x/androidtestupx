package com.example.androidtest.Data.FetchStrategies

import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.webkit.MimeTypeMap
import android.webkit.URLUtil
import com.example.androidtest.Data.Models.SingleData
import com.example.androidtest.Data.Models.UserModel
import com.google.gson.JsonObject
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * Implementation of a remote fetch strategy
 * Uses Retrofit
 */
class RemoteFetchStrategy: FetchStrategy() {

    private val apiUrl = "http://upaxdev.com/ws/webresources/generic/"

    //  Interface containig functions for api methods
    interface DataApi{
        @Headers("Content-Type: application/json")
        @POST("getData")
        fun getData(@Body body: JsonObject): Deferred<Response<SingleData>>
    }

    //  Retrofit init
    private val connectionClient = Retrofit.Builder()
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(apiUrl)
        .build()
    private val api = connectionClient.create(DataApi::class.java)

    override suspend fun addUser(user: UserModel): Boolean? {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return false
    }

    override suspend fun getAllUsers(): ArrayList<UserModel>? {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return ArrayList()
    }

    override suspend fun getData(): SingleData? {
        val parameters =JsonObject()
        parameters.addProperty("serviceId", 55)
        parameters.addProperty("userId", 12984)

        val dataResponse = executeFetchCall(
            call = {api.getData(parameters).await()},
            errorMessage = "Error in the response"
        )

        return dataResponse
    }

    override suspend fun getZipFileDownloadId(url: String): Long? {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return 0L
    }
}