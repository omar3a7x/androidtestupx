package com.example.androidtest.Data.FetchStrategies

import android.util.Log
import com.example.androidtest.Data.Models.SingleData
import com.example.androidtest.Data.Models.UserModel
import retrofit2.Response
import java.io.IOException

/**
 * Abstract Fetch Strategy
 * Includes the data fetch functions that must be implemented by concrete implementations
 */
abstract class FetchStrategy {

    abstract suspend fun getData(): SingleData?
    abstract suspend fun getZipFileDownloadId(url: String): Long?
    abstract suspend fun addUser(user: UserModel): Boolean?
    abstract suspend fun getAllUsers(): ArrayList<UserModel>?

    suspend fun <T : Any> executeFetchCall(call: suspend () -> Response<T>, errorMessage: String): T? {

        val result : FetchResult<T> = retrieveFetchResult(call, errorMessage)
        var data : T? = null

        when(result) {
            is FetchResult.Success ->
                data = result.data
            is FetchResult.Error -> {
                Log.d("GenericRepository", "$errorMessage & Exception - ${result.exception}")
            }
        }


        return data

    }

    private suspend fun <T: Any> retrieveFetchResult(call: suspend ()-> Response<T>, errorMessage: String) : FetchResult<T> {
        val response = call.invoke()
        if(response.isSuccessful) return FetchResult.Success(response.body()!!)

        return FetchResult.Error(IOException("Error Occurred during retrieveFetchResult, Custom ERROR - $errorMessage"))
    }
}
/**
 * Fetch result type
 * */
sealed class FetchResult<out T: Any> {
    data class Success<out T : Any>(val data: T) : FetchResult<T>()
    data class Error(val exception: Exception) : FetchResult<Nothing>()
}