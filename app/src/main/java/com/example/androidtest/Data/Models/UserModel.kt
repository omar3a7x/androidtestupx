package com.example.androidtest.Data.Models

data class UserModel(
    val name: String,
    val birthDate: String,
    val position: String
)