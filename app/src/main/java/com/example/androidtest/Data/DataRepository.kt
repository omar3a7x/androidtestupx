package com.example.androidtest.Data

import com.example.androidtest.Data.FetchStrategies.FetchStrategy
import com.example.androidtest.Data.Models.SingleData
import com.example.androidtest.Data.Models.UserModel

/**
 * Data repository for the app
 * Requires a concrete fetch implementation (Local/remote)
 * Defines the contracts for the app to fetch data
 */
class DataRepository(val fetchImplementation: FetchStrategy) {

    suspend fun getData(): SingleData?  {
        return fetchImplementation.getData()
    }

    suspend fun getZipFile(url: String): Long?  {
        return fetchImplementation.getZipFileDownloadId(url)
    }

    suspend fun addUser(user: UserModel): Boolean?  {
        return fetchImplementation.addUser(user)
    }

    suspend fun getAllUsers(): ArrayList<UserModel>?  {
        return fetchImplementation.getAllUsers()
    }
}