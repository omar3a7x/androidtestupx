package com.example.androidtest.Data.FetchStrategies

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.example.androidtest.Data.Models.SingleData
import com.example.androidtest.Data.Models.UserModel

class LocalFetchStrategy(val context: Context): FetchStrategy() {
    override suspend fun getData(): SingleData? {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return null
    }

    val sqLiteHelper = SQLiteHelper(context, null)

    override suspend fun getZipFileDownloadId(url: String): Long? {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return 0L
    }

    override suspend fun addUser(user: UserModel): Boolean {
        return sqLiteHelper.addUser(user)
    }

    override suspend fun getAllUsers(): ArrayList<UserModel>? {
        return sqLiteHelper.getAllUsers()
    }
}