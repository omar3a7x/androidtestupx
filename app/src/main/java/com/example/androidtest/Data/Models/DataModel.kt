package com.example.androidtest.Data.Models

data class SingleData(
    val code: Int,
    val valueResponse: String,
    val hasError: Boolean
)