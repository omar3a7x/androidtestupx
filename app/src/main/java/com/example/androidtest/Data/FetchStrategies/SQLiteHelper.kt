package com.example.androidtest.Data.FetchStrategies

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.example.androidtest.Data.Models.UserModel

class SQLiteHelper(context: Context, factory: SQLiteDatabase.CursorFactory?):
        SQLiteOpenHelper(context, DATABASE_NAME,
                factory, DATABASE_VERSION) {
    
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CREATE_USER_TABLE)
    }
    
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }
    
    fun addUser(user: UserModel): Boolean {
        // Gets the data repository in write mode
        val db = writableDatabase

        val values = ContentValues()
        values.put(COLUMN_NAME, user.name)
        values.put(COLUMN_BIRTHDATE, user.birthDate)
        values.put(COLUMN_POSITION, user.position)

        val newRowId = db.insert(TABLE_NAME, null, values)

        return true
    }
    fun getAllUsers(): ArrayList<UserModel>? {
        val users = ArrayList<UserModel>()
        val db = writableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery("select * from " + TABLE_NAME, null)
        } catch (e: SQLiteException) {
            db.execSQL(CREATE_USER_TABLE)
            return ArrayList()
        }

        var birthdate: String
        var name: String
        var position: String
        if (cursor!!.moveToFirst()) {
            while (cursor.isAfterLast == false) {
                name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME))
                birthdate = cursor.getString(cursor.getColumnIndex(COLUMN_BIRTHDATE))
                position = cursor.getString(cursor.getColumnIndex(COLUMN_POSITION))

                users.add(UserModel(name, birthdate, position))
                cursor.moveToNext()
            }
        }
        return users
    }
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "AndroidTest.db"
        val TABLE_NAME = "user"
        val COLUMN_ID = "user_id"
        val COLUMN_NAME = "name"
        val COLUMN_BIRTHDATE = "birthdate"
        val COLUMN_POSITION = "position"
        val CREATE_USER_TABLE = ("CREATE TABLE " +
                TABLE_NAME + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_NAME + " TEXT,"
                + COLUMN_BIRTHDATE + " TEXT,"
                + COLUMN_POSITION + " TEXT)")
    }
}