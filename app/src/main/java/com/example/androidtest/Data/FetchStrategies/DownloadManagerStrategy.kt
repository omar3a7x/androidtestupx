package com.example.androidtest.Data.FetchStrategies

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Environment
import android.webkit.MimeTypeMap
import android.webkit.URLUtil
import android.widget.Toast
import com.example.androidtest.Data.Models.SingleData
import com.example.androidtest.Data.Models.UserModel
import com.google.gson.JsonObject
import java.io.File

class DownloadManagerStrategy(val context: Context): FetchStrategy() {
    override suspend fun addUser(user: UserModel): Boolean? {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return false
    }

    override suspend fun getAllUsers(): ArrayList<UserModel>? {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return ArrayList()
    }

    override suspend fun getData(): SingleData? {
        return SingleData(0, "", false)
    }

    override suspend fun getZipFileDownloadId(url: String): Long? {
        val fileName = URLUtil.guessFileName(url, null, MimeTypeMap.getFileExtensionFromUrl(url))
        val request = DownloadManager.Request(Uri.parse(url))
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
        request.setTitle("Download")
        request.setDescription("The file is downloading")
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
        request.allowScanningByMediaScanner()
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)

        val manager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        val downloadID = manager.enqueue(request)

        return downloadID
    }
}