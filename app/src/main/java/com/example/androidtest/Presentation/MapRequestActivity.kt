package com.example.androidtest.Presentation

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidtest.Utils.PermissionsUtility
import java.io.FileOutputStream as FileOutputStream1
import android.view.View.GONE
import com.example.androidtest.Data.Models.MapMarkerModel
import com.example.androidtest.R
import com.example.androidtest.Utils.ProcessHandler
import com.example.androidtest.Utils.MarkerProcessingFacade
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_map_request.*
import kotlinx.android.synthetic.main.activity_map_request.fMap


class MapRequestActivity : AppCompatActivity(), OnMapReadyCallback {


    private var markers = ArrayList<MapMarkerModel>()

    private var mapFragment: SupportMapFragment? = null
    private lateinit var map: GoogleMap

    override fun onMapReady(map: GoogleMap?) {
        this.map = map!!

        if(!PermissionsUtility.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            PermissionsUtility.askPermissionIfNotGranted(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                PermissionsUtility.Codes.FOR_WRITE)
        } else if(!PermissionsUtility.hasPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            PermissionsUtility.askPermissionIfNotGranted(this,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                PermissionsUtility.Codes.FOR_READ)
        } else {
            getMarkers()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_request)

        mapFragment = (fMap as SupportMapFragment)
        mapFragment!!.getMapAsync(this)
    }

    private fun getMarkers() {
        //  Uses MarkerProcessingFacade to get markers list
        MarkerProcessingFacade(this, object : ProcessHandler {
            override fun processFinish(result: Any, error: String) {
                if(result is ArrayList<*>) {
                    markers = result as ArrayList<MapMarkerModel>

                    //  Add markers to map
                    val builder = LatLngBounds.Builder()
                    for(marker in markers) {
                        map.addMarker(MarkerOptions().position(LatLng(marker.latitude, marker.longitude)).title(marker.title))
                        builder.include(LatLng(marker.latitude, marker.longitude))
                    }

                    val bounds: LatLngBounds = builder.build()
                    val padding = 0; // offset from edges of the map in pixels
                    val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
                    map.moveCamera(cu)
                    pbLoading.visibility = GONE
                }
            }
        }).start()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode) {
            PermissionsUtility.Codes.FOR_WRITE.code -> {
                if(PermissionsUtility.isResultGranted(grantResults,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        this)) {
                    getMarkers()
                } else {
                    //  not allowed
                }
            }
        }
    }

}
