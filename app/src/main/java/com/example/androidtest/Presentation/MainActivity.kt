package com.example.androidtest.Presentation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidtest.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnMapInput.setOnClickListener {
            startActivity(Intent(this, MapInputActivity::class.java))
        }

        btnMapRequest.setOnClickListener {
            startActivity(Intent(this, MapRequestActivity::class.java))
        }

        btnThread.setOnClickListener {
            startActivity(Intent(this, ThreadActivity::class.java))
        }

        btnStorage.setOnClickListener {
            startActivity(Intent(this, StorageActivity::class.java))
        }
    }
}
