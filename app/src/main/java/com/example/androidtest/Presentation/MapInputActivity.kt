package com.example.androidtest.Presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.androidtest.Data.Models.MapMarkerModel
import com.example.androidtest.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_map_input.*

class MapInputActivity : AppCompatActivity(), OnMapReadyCallback {

    private var mapFragment: SupportMapFragment? = null
    private lateinit var map: GoogleMap

    override fun onMapReady(map: GoogleMap?) {
        this.map = map!!


        map.moveCamera(CameraUpdateFactory.newLatLng(LatLng(19.3910376, -99.2094695)))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar!!.hide()
        setContentView(R.layout.activity_map_input)

        mapFragment = (fMap as SupportMapFragment)
        mapFragment!!.getMapAsync(this)

        btnPaint.setOnClickListener {
            if(etInput.text.isEmpty()) {
                Toast.makeText(this, "Please enter a value", Toast.LENGTH_SHORT).show()
            } else {
                addMarkers(Integer.parseInt(etInput.text.toString()))
            }
        }
    }

    private fun addMarkers(numMarkers:Int) {
        //val markers = ArrayList<MapMarkerModel>()
        map.clear()
        val builder = LatLngBounds.Builder()
        for(i in 0 until numMarkers) {
            val newMarker = MapMarkerModel((14..35).random().toDouble(), (-113..-87).random().toDouble(), numMarkers.toString())
            map.addMarker(MarkerOptions().position(LatLng(newMarker.latitude, newMarker.longitude)).title(newMarker.title))
            builder.include(LatLng(newMarker.latitude, newMarker.longitude))
            //markers.add(newMarker)
        }
        val bounds: LatLngBounds = builder.build()
        val padding = 0; // offset from edges of the map in pixels
        val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
        map.moveCamera(cu)
    }
}
