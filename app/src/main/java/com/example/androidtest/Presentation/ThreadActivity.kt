package com.example.androidtest.Presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidtest.R
import android.widget.Toast
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import com.example.androidtest.Utils.CustomThread
import com.example.androidtest.Utils.ProcessHandler
import kotlinx.android.synthetic.main.activity_thread.*


class ThreadActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_thread)

        val btnCreateThread = Button(this)
        btnCreateThread.setText(R.string.create_thread)
        btnCreateThread.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        btnCreateThread.setOnClickListener{
            Thread(CustomThread(object: ProcessHandler {
                override fun processFinish(result: Any, error: String) {
                    runOnUiThread {
                        Toast.makeText(this@ThreadActivity, "Time's up!", Toast.LENGTH_SHORT).show()
                    }
                }

            })).start()
        }

        llContainer.addView(btnCreateThread)

    }
}
