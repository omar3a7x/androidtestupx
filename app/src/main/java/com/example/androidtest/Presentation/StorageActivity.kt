package com.example.androidtest.Presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.androidtest.Data.DataRepository
import com.example.androidtest.Data.FetchStrategies.LocalFetchStrategy
import com.example.androidtest.Data.FetchStrategies.SQLiteHelper
import com.example.androidtest.Data.Models.SingleData
import com.example.androidtest.Data.Models.UserModel
import com.example.androidtest.R
import kotlinx.android.synthetic.main.activity_persistence.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext
import android.R.id.edit
import android.content.SharedPreferences



class StorageActivity : AppCompatActivity() {

    //  Coroutine init
    private val coroutineContext: CoroutineContext get() = Job() + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    //  Mutable data fetched from respositories
    private var users = MutableLiveData<ArrayList<UserModel>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_persistence)

        val pref = applicationContext.getSharedPreferences("AppPrefs", 0)
        val firstInstall = pref.getBoolean("1stInstall", false)

        if(!firstInstall) {
            val user0 = UserModel("Miguel Cervantes", "08-Dic-1990", "Desarrollador")
            val user1 = UserModel("Juan Morales", "03-Jul-1990", "Desarrollador")
            val user2 = UserModel("Roberto Méndez", "14-Oct-1990", "Desarrollador")
            val user3 = UserModel("Miguel Cuevas", "08-Dic-1990", "Desarrollador")

            scope.launch {
                DataRepository(LocalFetchStrategy(this@StorageActivity)).addUser(user0)
                DataRepository(LocalFetchStrategy(this@StorageActivity)).addUser(user1)
                DataRepository(LocalFetchStrategy(this@StorageActivity)).addUser(user2)
                DataRepository(LocalFetchStrategy(this@StorageActivity)).addUser(user3)

                val editor = pref.edit()
                editor.putBoolean("1stInstall", true)
                editor.apply()

                getDataFromDB()
            }
        } else {
            getDataFromDB()
        }
    }

    fun getDataFromDB() {
        scope.launch {
            val usersResponse = DataRepository(LocalFetchStrategy(this@StorageActivity)).getAllUsers()

            users.postValue(usersResponse)
            users.observe(this@StorageActivity, Observer {
                users.value!!.forEach {
                    val strUser = it.name + " " + it.birthDate + " " + it.position + "\n"
                    tvUsers.text = tvUsers.text.toString() + strUser
                }
            })
        }
    }
}
